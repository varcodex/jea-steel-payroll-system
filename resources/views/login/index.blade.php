<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JEA Steel - Payroll System</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
    <div class="body"></div>
    <div class="grad"></div>
    <div class="header">
        <div>Hi, <span>Welcome</span></div>
    </div>
    <div class="login">
        @include('messages.flashmessage')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>

            </div><br/><br/>
        @endif
        <form action="/verifying" method="GET">
            <div class="input-group">
                <input type="text" placeholder="username" name="empid"><br>
                <input type="password" placeholder="password" name="pw"><br>
                <input type="submit" value="Login">
            </div>
        </form>
    </div>
</body>

</html>
