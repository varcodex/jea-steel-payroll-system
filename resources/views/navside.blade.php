<aside class="left-sidebar">
    <div class="d-flex no-block nav-text-box align-items-center">
        <span><img src="{{ asset('assets/images/logo-icon.png') }}" alt="elegant admin template"></span>
        <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)"><i class="ti-menu"></i></a>
        <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
    </div>
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="waves-effect waves-dark" href="/dashboard" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/timeattendance" aria-expanded="false"><i class="fa fa-clock-o"></i><span class="hide-menu">Time Attendance</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/payslip" aria-expanded="false"><i class="fa fa-file"></i><span class="hide-menu">Payslip</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/leave" aria-expanded="false"><i class="fa fa-clipboard"></i><span class="hide-menu">Leave</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/schedule" aria-expanded="false"><i class="fa fa-calendar"></i><span class="hide-menu">Schedule</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/overtime" aria-expanded="false"><i class="fa fa-file-text"></i><span class="hide-menu">Overtime</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/loan" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Loan</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                <li> <a class="waves-effect waves-dark" href="/" aria-expanded="false"><i class="fa fa-arrow-circle-left"></i><span class="hide-menu">Logout</span></a></li>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
