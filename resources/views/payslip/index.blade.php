@extends('layout')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Payslip</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                    <li class="breadcrumb-item active">Payslip</li>
                </ol>
                </div>
        </div>
    </div>

    <div class="row">
        <!-- column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee No.</th>
                                    <th>Employee Name</th>
                                    <th>Payperiod</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($datas['payperiod'] as $data)
                                    <tr>
                                        <td>{{ $data['recid'] }}</td>
                                        <td>{{ $data['empid'] }}</td>
                                        <td>{{ $data['name___'] }}</td>
                                        <td>{{ $data['payperiod'] }}</td>
                                        <td><a href="/payslip/{{ $data['recid'] }}"><label class="label label-success"><i class="fa fa-eye"></i></label></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
