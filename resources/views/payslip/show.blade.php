@extends('layout')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Payslip</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                    <li class="breadcrumb-item"><a href="/payslip">Payslip</a></li>
                    <li class="breadcrumb-item active">Details</li>
                </ol>
                </div>
        </div>
    </div>

    <div class="row">
        @foreach($datas['paydetails'] as $data)
            <!-- Profile -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>

                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td class="font-weight-normal">Name:</td>
                                            <td class="font-weight-light">{{ $data['name___'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Emp ID:</td>
                                            <td class="font-weight-light">{{ $data['empid'] }}</td>
                                            <td class="font-weight-normal">Prepared by:</td>
                                            <td class="font-weight-light">{{ $data['encoder'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">Pay Period:</td>
                                            <td class="font-weight-light">{{ $data['payperiod'] }}</td>
                                            <td class="font-weight-normal">Pay type:</td>
                                            <td class="font-weight-light">{{ $data['paytype'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Period:</td>
                                            <td class="font-weight-light">{{ $data['period'] }}</td>
                                            <td class="font-weight-normal">Num:</td>
                                            <td class="font-weight-light">{{ $data['num'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">Pay Date:</td>
                                            <td class="font-weight-light">{{ $data['pay_date'] }}</td>
                                            <td class="font-weight-normal">From:</td>
                                            <td class="font-weight-light">{{ $data['from_date'] }}</td>
                                            <td class="font-weight-normal">Up to:</td>
                                            <td class="font-weight-light">{{ $data['upto_date'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Recid:</td>
                                            <td class="font-weight-light">{{ $data['recid'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">Branch:</td>
                                            <td class="font-weight-light">{{ $data['brname'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Dept:</td>
                                            <td class="font-weight-light">{{ $data['dept'] }}-{{ $data['deptname'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="font-weight-bold">Earnings</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td class="font-weight-normal">Rateday:</td>
                                            <td class="font-weight-light">{{ $data['rateday'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">Dayswork:</td>
                                            <td class="font-weight-light">{{ $data['dayswork'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">OT Reg. Hr.:</td>
                                            <td class="font-weight-light">{{ $data['otreghr'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">OT Spe. Hr.:</td>
                                            <td class="font-weight-light">{{ $data['otsunhr'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">OT Hol. Hr.:</td>
                                            <td class="font-weight-light">{{ $data['otholhr'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">OT ND. Hr.:</td>
                                            <td class="font-weight-light">{{ $data['otndhr'] }}</td>
                                            <td class="font-weight-normal">Spe/Hol/ND:</td>
                                            <td class="font-weight-light">{{ $data['e03'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Hol w/ Pay:</td>
                                            <td class="font-weight-light">{{ $data['e04'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Leave w/ Pay:</td>
                                            <td class="font-weight-light">{{ $data['e05'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">COLA:</td>
                                            <td class="font-weight-light">{{ $data['e06'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Meal Allow.:</td>
                                            <td class="font-weight-light">{{ $data['e07'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Load Allow.:</td>
                                            <td class="font-weight-light">{{ $data['e08'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">P.I.A.:</td>
                                            <td class="font-weight-light">{{ $data['e09'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">T.E.R.A.:</td>
                                            <td class="font-weight-light">{{ $data['e10'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">SLVL Convers.:</td>
                                            <td class="font-weight-light">{{ $data['e11'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-bold">Total:</td>
                                            <td class="font-weight-light">{{ $data['totamt'] }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Adjustments -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="font-weight-bold">Adjustments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td class="font-weight-normal">Basic Pay:</td>
                                            <td class="font-weight-light">{{ $data['a01'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">COLA:</td>
                                            <td class="font-weight-light">{{ $data['a02'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">SSS Loan:</td>
                                            <td class="font-weight-light">{{ $data['a03'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">OT:</td>
                                            <td class="font-weight-light">{{ $data['a04'] }}</td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Deductions -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="font-weight-bold">Deductions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td class="font-weight-normal">Tard. Hrs/Min:</td>
                                            <td class="font-weight-light">{{ $data['tardhr'] }}</td>
                                            <td class="font-weight-normal">Tard. Amt.:</td>
                                            <td class="font-weight-light">{{ $data['d01'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-normal">Absent Days:</td>
                                            <td class="font-weight-light">{{ $data['absday'] }}</td>
                                            <td class="font-weight-normal">Abs. Amt:</td>
                                            <td class="font-weight-light">{{ $data['d02'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Other:</td>
                                            <td class="font-weight-light">{{ $data['d03'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">SSS Cont.:</td>
                                            <td class="font-weight-light">{{ $data['d04'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">P.Ibig Cont.:</td>
                                            <td class="font-weight-light">{{ $data['d05'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Philhealth:</td>
                                            <td class="font-weight-light">{{ $data['d06'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">SSS Loan:</td>
                                            <td class="font-weight-light">{{ $data['d07'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">SSS Loan:</td>
                                            <td class="font-weight-light">{{ $data['d08'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Salary Loan:</td>
                                            <td class="font-weight-light">{{ $data['d09'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Cash Adv.:</td>
                                            <td class="font-weight-light">{{ $data['d10'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">Telephone:</td>
                                            <td class="font-weight-light">{{ $data['d11'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-normal">WHTax:</td>
                                            <td class="font-weight-light">{{ $data['wht'] }}</td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-bold">TOTAL:</td>
                                            <td class="font-weight-light">{{ $data['totded'] }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-light"></td>
                                            <td class="font-weight-bold">NET PAY:</td>
                                            <td class="font-weight-light">{{ $data['netamt'] }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
