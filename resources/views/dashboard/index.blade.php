@extends('layout')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Dashboard</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
                </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card oh">
                <div class="card-body">
                    <div class="d-flex m-b-30 align-items-center no-block">
                        <h5 class="card-title ">Time Attendance 2019</h5>
                        <div class="ml-auto">
                            <ul class="list-inline font-12">
                                <li><i class="fa fa-circle text-info"></i> Present</li>
                                <li><i class="fa fa-circle text-primary"></i> Absent</li>
                            </ul>
                        </div>
                    </div>
                    <div id="morris-area-chart" style="height: 350px;"></div>
                </div>

            </div>
        </div>
    </div>

@endsection
