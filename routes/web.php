<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',
            ['uses' => 'LoginController@index']);

Route::get('verifying',
            ['uses' => 'LoginController@checkLogin']);

//Dashboard
Route::get('dashboard',
            ['uses' => 'DashboardController@index'])->name('dashboard');

//Leave
Route::get('leave',
            ['uses' => 'LeaveController@index']);

//Loan
Route::get('loan',
            ['uses' => 'LoanController@index']);

//Overtime
Route::get('overtime',
            ['uses' => 'OvertimeController@index']);

//Payslip
Route::get('payslip',
            ['uses' => 'PayslipController@index']);
Route::get('payslip/{recid}',
            ['uses' => 'PayslipController@show']);

//Profile
Route::get('profile',
            ['uses' => 'ProfileController@index']);

//Schedule
Route::get('schedule',
            ['uses' => 'ScheduleController@index']);

//TimeAttendance
Route::get('timeattendance',
            ['uses' => 'timeattendanceController@index']);
