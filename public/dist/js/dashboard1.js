$(function() {
    "use strict";
    // Dashboard 1 Morris-chart
    Morris.Bar({
        element: 'morris-area-chart',
        data:   [
                    {"period": "July", "present": 28, "absent": 0},
                    {"period": "June", "present": 18, "absent": 10},
                    {"period": "May", "present": 26, "absent": 2},
                    {"period": "April", "present": 28, "absent": 0},
                    {"period": "March", "present": 25, "absent": 3},
                    {"period": "February", "present": 28, "absent": 0},
                    {"period": "January", "present": 28, "absent": 0}
                ],
        xkey: 'period',
        ykeys: ['present', 'absent'],
        labels: ['Present', 'Absent'],
        pointSize: 0,
        fillOpacity: 1,
        pointStrokeColors: ['#f62d51', '#7460ee', '#009efb'],
        behaveLikeLine: true,
        gridLineColor: '#f6f6f6',
        lineWidth: 2,
        hideHover: 'auto',
        barColors: ['#009efb', '#7460ee', '#009efb'],
        resize: true
    });

});
