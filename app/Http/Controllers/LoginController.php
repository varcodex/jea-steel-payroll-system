<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index');
    }

    public function checkLogin(Request $request)
    {

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post('http://localhost:8080/payroll/v1/checkLogin.php', [
            'form_params' => [
                'empid' => $request->get('empid'),
                'pw' => $request->get('pw')
            ]
        ]);

        $datas=json_decode($result->getBody()->getContents(),true);

        foreach ($datas as $data){
            if($data == null){
                Session::flash('flash_message', 'Invalid Username/Password');
                Session::flash('flash_type', 'alert-danger');
                return view('login.index');
            }else{
                foreach ($data['0'] as $key=>$empid){
                    if($key == 'empid'){
                        Session::put('empid', $empid);
                        return redirect()->route('dashboard');
                    }
                }
            }
        }
    }
}
